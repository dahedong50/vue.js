Vue.component('todo-id', {
    props: ['todo'],
    template: '<li>{{ todo.text }}</li>'
})

let app = new Vue({
    el: '#app',
    data:{
        id:[], text:""
    },
    methods:{
        addItem:function(event){
            if(this.text == '') {
                return;
            }
            let todo = {
                item: this.text
            };
            this.id.push(todo);
            this.text = '';
        }
    }
})